﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GameTrueFalse
{
    public partial class Form1 : Form
    {
        int Totalpoint = 0;
        int Bestscore;
        int timeCount;
        Random rnd = new Random();
        bool RandomBool;
        Timer timer = new Timer();              
        public Form1()
        {
            InitializeComponent();
            btnFalse.Hide();
            btnTrue.Hide();
            btnStart.Click += BtnStart_Click;
            lblPoint.Hide();
            timeLabel.Hide();
            btnTrue.Click += BtnTrue_Click;
            btnFalse.Click += BtnFalse_Click;
            timer.Tick += Timer_Tick;
            timer.Interval = (2);
            PrTime.Visible = false;
            lblGameOver.Hide();
            lblBS.Hide();
            Background.Show();
            PrTime.Maximum = 2000;
            PrTime.Minimum = 0;
            PrTime.Step = 18;
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            if (timeCount > 0)
            {
                timeCount--;
                timeLabel.Text = "Time left:";                               
                PrTime.PerformStep();
            }
            else
            {
                EndGame();
            }
        }
        public void RandomMath()
        {
            timeLabel.Show();
            timeCount = 150;
            btnTrue.Show();
            btnFalse.Show();
            lblBS.Text = "BestScore:" + Bestscore;
            lblPoint.Text = "Point:" + Totalpoint;
            lblPoint.Show();
            Random rnd = new Random();
            int x = rnd.Next(1, 10);
            int y = 10 - x;
            
            if (Totalpoint > 10)
            {                           // when user get over 10 point difficulty level will be raised
                timeCount = 100;
                PrTime.Step = 32; 
                x = rnd.Next(1, 20);
                y = 20 - x;
            }
            RandomBool = GetRandomBoolean();
            int operation = rnd.Next(1, 4);
            string operatorString;
            float answer;
            switch (operation)
            {
                case 1:
                    answer = x + y;
                    operatorString = "+";
                    break;
                case 2:
                    answer = x - y;
                    operatorString = "-";
                    break;
                case 3:
                    answer = x * y;
                    operatorString = "*";
                    break;
                case 4:
                    answer = x / y;
                    operatorString = "/";
                    break;
                default:
                    answer = 0;
                    operatorString = "?";
                    break;
            }

            if (RandomBool)
            {
                lblMath.Text = x + " " + operatorString + " " + y + " = " + answer + " ?";

            }
            else
            {
                int RandomFault = GiveMeDiffAns((int)answer);
                lblMath.Text = x + " " + operatorString + " " + y + " = " + RandomFault + " ?";
            }

            lblMath.Show();
        }
        private void BtnFalse_Click(object sender, EventArgs e)
        {
            timer.Start();
            if (RandomBool == false)
            {
                Totalpoint++;
                lblPoint.Text = "Point:" + Totalpoint;
                RandomMath();
                PrTime.Value = 1;

            }
            else
            {
                EndGame();
                PrTime.Hide();
                lblGameOver.Show();
                Background.Show();
            }
        }
        private void BtnTrue_Click(object sender, EventArgs e)
        {
            timer.Start();
            if (RandomBool)
            {
                Totalpoint++;
                lblPoint.Text = "Point:" + Totalpoint;
                RandomMath();
                PrTime.Value = 1;

            }
            else
            {
                EndGame();
                PrTime.Hide();
                lblGameOver.Show();
                Background.Show();
            }
        }
        public void EndGame()
        {
            if (Totalpoint > Bestscore)
            {
                lblMath.Text = "New Record: " + Totalpoint;
                Bestscore = Totalpoint;
            }
            else
            {
                lblMath.Text = "Point:" + Totalpoint;
            }
            lblPoint.Hide();
            timeLabel.Hide();
            lblBS.Hide();
            btnStart.Text = "Back";
            btnStart.Show();
            back = true;
            btnTrue.Hide();
            btnFalse.Hide();
            timer.Stop();
            PrTime.Hide();
            lblGameOver.Show();
            Background.Show();
            PrTime.Maximum = 2000;  // return level time
            PrTime.Minimum = 0;
            PrTime.Step = 18;
        }
        private int GiveMeDiffAns(int point)
        {
            int tmp = rnd.Next(point - 10, point + 10);
            if (tmp == point)
            {
                return GiveMeDiffAns(point);
            }
            else return tmp;
        }
        public static bool GetRandomBoolean()
        {
            return new Random().Next(100) % 2 == 0; // random

        }
        bool back = false;
        private void BtnStart_Click(object sender, EventArgs e)
        {
            PrTime.Visible = true;
            PrTime.Value = 1;  // reset time slider
            Background.Hide();
            if (back)
            {
                lblMath.Text = "Press Start";
                btnStart.Text = "Start";
                btnStart.Show();
                btnFalse.Hide();
                btnTrue.Hide();
                lblBS.Hide();
                back = false;
                PrTime.Visible = false;
                lblGameOver.Hide();
                Background.Show();
            }
            else
            {
                btnStart.Hide();
                Totalpoint = 0;
                lblBS.Show();
                RandomMath();
            }
        }
    }
}

