﻿namespace GameTrueFalse
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.lblGameOver = new System.Windows.Forms.Label();
            this.PrTime = new System.Windows.Forms.ProgressBar();
            this.lblBS = new System.Windows.Forms.Label();
            this.timeLabel = new System.Windows.Forms.Label();
            this.lblPoint = new System.Windows.Forms.Label();
            this.lblMath = new System.Windows.Forms.Label();
            this.btnFalse = new System.Windows.Forms.Button();
            this.btnTrue = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.Background = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.Background)).BeginInit();
            this.SuspendLayout();
            // 
            // lblGameOver
            // 
            this.lblGameOver.AutoSize = true;
            this.lblGameOver.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGameOver.Location = new System.Drawing.Point(31, 19);
            this.lblGameOver.Name = "lblGameOver";
            this.lblGameOver.Size = new System.Drawing.Size(224, 46);
            this.lblGameOver.TabIndex = 17;
            this.lblGameOver.Text = "Game Over";
            // 
            // PrTime
            // 
            this.PrTime.Location = new System.Drawing.Point(12, 35);
            this.PrTime.Name = "PrTime";
            this.PrTime.Size = new System.Drawing.Size(260, 14);
            this.PrTime.TabIndex = 16;
            // 
            // lblBS
            // 
            this.lblBS.AutoSize = true;
            this.lblBS.Location = new System.Drawing.Point(107, 19);
            this.lblBS.Name = "lblBS";
            this.lblBS.Size = new System.Drawing.Size(56, 13);
            this.lblBS.TabIndex = 15;
            this.lblBS.Text = "BestScore";
            // 
            // timeLabel
            // 
            this.timeLabel.AutoSize = true;
            this.timeLabel.Location = new System.Drawing.Point(23, 19);
            this.timeLabel.Name = "timeLabel";
            this.timeLabel.Size = new System.Drawing.Size(30, 13);
            this.timeLabel.TabIndex = 14;
            this.timeLabel.Text = "Time";
            // 
            // lblPoint
            // 
            this.lblPoint.AutoSize = true;
            this.lblPoint.Location = new System.Drawing.Point(210, 19);
            this.lblPoint.Name = "lblPoint";
            this.lblPoint.Size = new System.Drawing.Size(34, 13);
            this.lblPoint.TabIndex = 13;
            this.lblPoint.Text = "Point:";
            // 
            // lblMath
            // 
            this.lblMath.AutoSize = true;
            this.lblMath.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.lblMath.Location = new System.Drawing.Point(64, 84);
            this.lblMath.Name = "lblMath";
            this.lblMath.Size = new System.Drawing.Size(149, 31);
            this.lblMath.TabIndex = 12;
            this.lblMath.Text = "Press Start";
            // 
            // btnFalse
            // 
            this.btnFalse.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFalse.Location = new System.Drawing.Point(161, 198);
            this.btnFalse.Name = "btnFalse";
            this.btnFalse.Size = new System.Drawing.Size(111, 41);
            this.btnFalse.TabIndex = 11;
            this.btnFalse.Text = "✗";
            this.btnFalse.UseVisualStyleBackColor = true;
            // 
            // btnTrue
            // 
            this.btnTrue.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTrue.Location = new System.Drawing.Point(12, 198);
            this.btnTrue.Name = "btnTrue";
            this.btnTrue.Size = new System.Drawing.Size(114, 41);
            this.btnTrue.TabIndex = 10;
            this.btnTrue.Text = "✓";
            this.btnTrue.UseVisualStyleBackColor = true;
            // 
            // btnStart
            // 
            this.btnStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStart.Location = new System.Drawing.Point(80, 133);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(120, 59);
            this.btnStart.TabIndex = 9;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            // 
            // Background
            // 
            this.Background.Image = ((System.Drawing.Image)(resources.GetObject("Background.Image")));
            this.Background.Location = new System.Drawing.Point(2, 8);
            this.Background.Name = "Background";
            this.Background.Size = new System.Drawing.Size(281, 264);
            this.Background.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Background.TabIndex = 18;
            this.Background.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.lblGameOver);
            this.Controls.Add(this.PrTime);
            this.Controls.Add(this.lblBS);
            this.Controls.Add(this.timeLabel);
            this.Controls.Add(this.lblPoint);
            this.Controls.Add(this.lblMath);
            this.Controls.Add(this.btnFalse);
            this.Controls.Add(this.btnTrue);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.Background);
            this.Name = "Form1";
            this.Text = "GameTrueFalse";
            ((System.ComponentModel.ISupportInitialize)(this.Background)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblGameOver;
        private System.Windows.Forms.ProgressBar PrTime;
        private System.Windows.Forms.Label lblBS;
        private System.Windows.Forms.Label timeLabel;
        private System.Windows.Forms.Label lblPoint;
        private System.Windows.Forms.Label lblMath;
        private System.Windows.Forms.Button btnFalse;
        private System.Windows.Forms.Button btnTrue;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.PictureBox Background;
    }
}

